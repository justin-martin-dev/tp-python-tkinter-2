from functools import partial
from tkinter import *
from tkinter import messagebox

expression = ""

def pressOperator(operator):
    if operator == "=":
        equal()
    else:
        press(operator)

def press(num):
    global expression
    expression = expression + str(num)
    equation.set(expression)

def equal():
    try:
        global expression
        print(expression)
        total = str(eval(expression))
        equation.set(total)
        expression = ""
    except:
        equation.set(" error ")
        expression = ""

def C():
    global expression
    expression = expression[:-1]
    equation.set(expression)

def AC():
    global expression
    expression = ""
    equation.set("")

window = Tk()
window.title("Python Calculator")
window.geometry("220x450")
window.config(background="Dark grey")

equation = StringVar()

expression_field = Entry(window, textvariable=equation, font=("Calibri", 18), justify="right", width=18)
expression_field.place(x=0, y=0)
equation.set("")

def show_btn():
    numpad = [["(", ")"], [7, 8, 9], [4, 5, 6], [1, 2, 3], [-1, 0, "."]]
    y_dynamic = 100
    for row in numpad:
        x_dynamic = 10
        for num in row:
            if num == -1:
                x_dynamic += 50
                continue
            button = Button(window, text=str(num), fg='black', bg='white', padx=14, pady=14, bd=4, command=partial(press, num))
            button.place(x=x_dynamic, y=y_dynamic)
            x_dynamic += 50
        y_dynamic += 70

def show_operator():
    operators = ["%", "/", "*", "-", "+", "="]
    y_dynamic = 35
    for operator in operators:
        button = Button(window, text=str(operator), fg='black', bg='gainsboro', padx=14, pady=14, bd=4, command=partial(pressOperator, operator))
        button.place(x=165, y=y_dynamic)
        y_dynamic += 68

def show_clean_btn():
    button_ac = Button(window, text=str("AC"), fg='black', bg='firebrick1', padx=14, pady=14, bd=4, command=lambda:AC())
    button_ac.place(x=10, y=35)

    button_c = Button(window, text=str("C"), fg='black', bg='firebrick1', padx=14, pady=14, bd=4, command=lambda:C())
    button_c.place(x=75, y=35)

def show_menu():
    menubar = Menu(window)
    menubar.add_command(label="About", command=lambda:showAboutDialog())
    window.config(menu=menubar)

def showAboutDialog():
    messagebox.showinfo("About message box", "Calculatrice Python by Justin MARTIN")

show_btn()
show_operator()
show_clean_btn()
show_menu()

window.mainloop()
